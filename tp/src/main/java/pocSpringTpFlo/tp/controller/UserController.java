package pocSpringTpFlo.tp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import pocSpringTpFlo.tp.model.User;

@RequestMapping("poc")
@RestController
public class UserController {

    @Autowired
    private RestTemplate restTemplate;

    private final String URL_API = "https://jsonplaceholder.typicode.com/users";

    @GetMapping("/users/{id}")
    public User getUserById(@PathVariable Integer id) {
        String url = URL_API + "/{id}";
        return restTemplate.getForObject(url, User.class, id);
    }

    @GetMapping("/users")
    public User[] getAllUser() {
        return restTemplate.getForObject(URL_API, User[].class);

    }
}
