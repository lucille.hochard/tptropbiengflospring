package pocSpringTpFlo.tp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class User implements Serializable {
    private final int id;
    private final String name;
    private final String username;
    private final String email;

    public User(@JsonProperty("id") int id,
                @JsonProperty("name") String name,
                @JsonProperty("username") String username,
                @JsonProperty("email") String email) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }


}
